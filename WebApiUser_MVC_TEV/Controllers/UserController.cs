﻿using System.Collections.Generic;
using System.Web.Http;
using WebApiUser_MVC_TEV.Models;
using Interface;

namespace WebApiUser_MVC_TEV.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserData userManagement;

        public UserController(IUserData iUserData)
        {
            userManagement = iUserData;
        }

        // GET: api/User
        [HttpGet]
        [Route("api/User/GetAll")]
        public List<User> GetAll()
        {
            return userManagement.GetAll();
        }

        // GET: api/User/id
        [HttpGet]
        [Route("api/User/GetUser")]
        public IHttpActionResult GetUser(int id)
        {
            var user = userManagement.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        // POST: api/User
        [HttpPost]
        [Route("api/User/Create", Name = "Create")]
        public IHttpActionResult Create(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            userManagement.CreateUser(user);
            return CreatedAtRoute("Create", new { id = user.ID }, user);
        }

        // PUT: api/User/id
        [HttpPost]
        [Route("api/User/Update")]
        public IHttpActionResult Update(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(userManagement.UpdateUser(id, user));
        }

        // DELETE: api/User/id
        [HttpPost]
        [Route("api/User/Delete")]
        public IHttpActionResult Delete(int id)
        {
            userManagement.DeleteUser(id);
            var user = userManagement.GetUser(id);
            if (user != null)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
