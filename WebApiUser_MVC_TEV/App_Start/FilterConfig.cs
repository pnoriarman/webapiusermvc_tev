﻿using System.Web;
using System.Web.Mvc;

namespace WebApiUser_MVC_TEV
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
