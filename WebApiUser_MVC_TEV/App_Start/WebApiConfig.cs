﻿using System.Web.Http;
using Unity;
using Unity.Lifetime;
using WebApiUser_MVC_TEV.Management;
using Interface;

namespace WebApiUser_MVC_TEV
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<IUserData, UserData>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver.UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

