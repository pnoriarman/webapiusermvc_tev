﻿namespace WebApiUser_MVC_TEV.DbContext
{
    using System.Data.Entity;
    using WebApiUser_MVC_TEV.Models;

    public partial class UserDBContext : DbContext
    {
        public virtual DbSet<User> User { get; set; }     
    }
}