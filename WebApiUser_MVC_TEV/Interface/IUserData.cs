﻿using System.Collections.Generic;
using WebApiUser_MVC_TEV.Models;

namespace Interface
{
    public interface IUserData
    {
        List<User> GetAll();
        User GetUser(int id);
        void CreateUser(User user);
        User UpdateUser(int id, User user);
        void DeleteUser(int id);
    }
}
