﻿using System.Collections.Generic;
using System.Linq;
using Interface;
using WebApiUser_MVC_TEV.Models;
using WebApiUser_MVC_TEV.DbContext;

namespace WebApiUser_MVC_TEV.Management
{
    public class UserData : IUserData
    {
        private UserDBContext userDBContext = new UserDBContext();

        public List<User> GetAll()
        {
            return userDBContext.User.ToList();
        }

        public User GetUser(int id)
        {
            return userDBContext.User.FirstOrDefault(x => x.ID == id);
        }

        public void CreateUser(User user)
        {
            userDBContext.User.Add(user);
            userDBContext.SaveChanges();
        }

        public User UpdateUser(int id, User data)
        {
            User user = userDBContext.User.FirstOrDefault(x => x.ID == id);
            user.Name = data.Name;
            user.LastName = data.LastName;
            user.Address = data.Address;
            user.UpdateDate = data.UpdateDate;
            userDBContext.SaveChanges();
            return user;
        }

        public void DeleteUser(int id)
        {
            User user = userDBContext.User.FirstOrDefault(x => x.ID == id);
            if (user != null)
            {
                userDBContext.User.Remove(user);
                userDBContext.SaveChanges();
            }
        }
    }
}