--1.- Seleccionar todos los clientes ordenados por nombre del cliente
SELECT * FROM dbo.Clientes ORDER BY NOM_CLIENTE

--2.- Seleccionar los clientes cuya localidad sea Madrid
SELECT * FROM dbo.Clientes WHERE LOC_CLIENTE = 'Madrid'

--3.- Seleccionar todos los clientes cuyo c�digo postal pertenezca a la provincia de Sevilla (los que comienzan por 41)
SELECT * FROM dbo.Clientes WHERE CP_CLIENTE LIKE '41%'

--4.- Seleccionar todos los clientes cuyo c�digo sea inferior a 10 o cuyo nombre de cliente comience por la letra A
SELECT * FROM dbo.Clientes WHERE (COD_CLIENTE < 10 OR NOM_CLIENTE LIKE 'A%')

--5.- Contar cu�ntos clientes existen por cada localidad
SELECT LOC_CLIENTE, COUNT(*) FROM dbo.Clientes GROUP BY LOC_CLIENTE

--6.- Borrar los clientes cuyo nombre comience por la letra Z y termine por la letra Z
DELETE FROM dbo.Clientes WHERE NOM_CLIENTE LIKE 'Z%z'

--7.- Modificar el valor del c�digo postal de los clientes cuya localidad sea Madrid con el valor 28001
UPDATE dbo.Clientes SET CP_CLIENTE = '28001' WHERE LOC_CLIENTE = 'Madrid'

--8.- Seleccionar los clientes cuyo importe de sus VENTAS sea distinto de cero.
SELECT CL.COD_CLIENTE, CL.NOM_CLIENTE, CL.CP_CLIENTE, CL.LOC_CLIENTE 
FROM dbo.Clientes Clientes
INNER JOIN dbo.Ventas Ventas ON Clientes.COD_CLIENTE = Ventas.COD_CLIENTE 
WHERE Ventas.IMPORTE != 0

--9.- Seleccionar todos los clientes junto con el importe de las ventas de cada uno.
SELECT Clientes.COD_CLIENTE, Clientes.NOM_CLIENTE, Clientes.CP_CLIENTE, Clientes.LOC_CLIENTE, Ventas.IMPORTE 
FROM dbo.Clientes Clientes
INNER JOIN dbo.Ventas Ventas ON Clientes.COD_CLIENTE = Ventas.COD_CLIENTE

--10.- Modificar el importe de las ventas de los clientes a cero para aquellos clientes cuyo nombre contenga al menos una letra A.
UPDATE dbo.Ventas SET IMPORTE = 0 
FROM dbo.Ventas Ventas INNER JOIN dbo.Clientes Clientes ON Clientes.COD_CLIENTE = Ventas.COD_CLIENTE 
WHERE Clientes.NOM_CLIENTE LIKE '%a%'
